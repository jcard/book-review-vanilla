package com.dotdash.recruiting.bookreview;

import java.io.IOException;
import java.io.Writer;
import java.util.Stack;

/**
 * Usually I'd use a library like Jackson or Gson to deal with JSON serialization,
 * but to keep things really minimal for the purposes of this exercise I'm just
 * doing it by hand.
 */
public class JsonWriter {
    // Stack used to track if we need a comma or not
    Stack<Boolean> propertiesWritten = new Stack<>();
    private Writer writer;

    public JsonWriter(Writer writer) {
        this.writer = writer;
    }

    public void startArray() throws IOException {
        writer.append('[');
        propertiesWritten.push(false);
    }

    public void endArray() throws IOException {
        writer.append(']');
        propertiesWritten.pop();
    }

    public void startObject() throws IOException {

        if(propertiesWritten.size() > 0 && propertiesWritten.peek()) {
            writer.append(',');
        } else if(propertiesWritten.size() > 0 && !propertiesWritten.peek()) {
            propertiesWritten.pop();
            propertiesWritten.push(true);
        }
        propertiesWritten.push(false);
        writer.append('{');
    }

    public void endObject() throws IOException {
        writer.append('}');
        propertiesWritten.pop();
    }

    /**
     * @param name
     *  Expected to already be sanitized and legal JSON property name. No input validation is performed.
     * @param value
     *  May contain non-legal characters for JSON that would need to be escaped.
     * @throws IOException
     *  Unable to use writer
     */
    public void writeProperty(String name, String value) throws IOException {
        writePropertyName(name)
                .append("\"")
                .append(escapeJsonValue(value))
                .append("\"");
    }

    public void writeIntProperty(String name, int value) throws IOException {
        writePropertyName(name)
                .append(Integer.toString(value));
    }

    private Writer writePropertyName(String name) throws IOException {
        if(propertiesWritten.size() > 0 && propertiesWritten.peek()) {
            writer.append(',');
        } else if(propertiesWritten.size() > 0){
            propertiesWritten.pop();
            propertiesWritten.push(true);
        }
        writer.append('"')
                .append(name)
                .append("\":");
        return writer;
    }

    private String escapeJsonValue(String value) {
        // This is *not* a complete escaping function. Again, if this were a "real" system I'd be using Jackson or
        // Gson to deal with serialization of JSON.
        String result = value;
        result = result.replace("\\", "\\\\");
        result = result.replace("\"", "\\\"");
        return result;
    }
}
