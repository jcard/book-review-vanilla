package com.dotdash.recruiting.bookreview;

/**
 * Plain-Jane version of a Book Search application backed by the GoodReads search API. This has been written
 * with footprint and performance in mind. In fact, the only direct dependencies are Jetty and the Jetty Client.
 * This is in pretty strong contrast to a full-blown JavaEE or Spring + Kitchen Sink kind of solution. This is
 * a solution that places greater value on scalability and performance than on ease of development. Something
 * like this might be appropriate in some contexts, and not suitable in many others.
 */
public class Main {
    public static void main(String[] args) {
        BookReviewServer.getInstance().start();
    }
}
