package com.dotdash.recruiting.bookreview;

/**
 * Generic exception thrown when an error occurs while we're processing a client request. 
 */
public class RequestException extends RuntimeException {

    public RequestException(String message) {
        super(message);
    }

    public RequestException(Exception ex) {
        super(ex);
    }

    public RequestException(String message, Exception ex) {
        super(message, ex);
    }
}
