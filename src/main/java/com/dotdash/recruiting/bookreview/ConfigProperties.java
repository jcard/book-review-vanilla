package com.dotdash.recruiting.bookreview;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigProperties {
    private static final String configFileName = "config.properties";

    private static ConfigProperties instance = new ConfigProperties();

    private final int serverPort;
    private final String goodreadsKey;
    private final String goodreadsBaseUrl;

    private ConfigProperties() {
        Properties properties = new Properties();
        InputStream stream = ClassLoader.getSystemResourceAsStream(configFileName);
        if(stream == null) {
            throw new RuntimeException("No file found.");
        }
        try {
            properties.load(stream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        serverPort = Integer.parseInt(properties.getProperty("server.port"));
        goodreadsKey = properties.getProperty("goodreads.key");
        goodreadsBaseUrl = properties.getProperty("goodreads.baseUrl");
    }

    public static ConfigProperties getInstance() {
        return instance;
    }

    public int getServerPort() {
        return serverPort;
    }

    public String getGoodreadsKey() {
        return goodreadsKey;
    }

    public String getGoodreadsBaseUrl() {
        return goodreadsBaseUrl;
    }
}
