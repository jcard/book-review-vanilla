package com.dotdash.recruiting.bookreview;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;

public class BookSearchResponseMapper {
    // Goodreads API won't return results past 2000.
    // We will cap the number of books to try to fetch at 2k.
    public static final int MAX_RESULTS = 2000;
    private boolean pageWritten = false;
    private JsonWriter writer;

    public BookSearchResponseMapper(Writer responseWriter) {
        this.writer = new JsonWriter(responseWriter);
    }

    /**
     * Write a single page from goodreads to the response. Note that, for a given request to our system, we may be
     * serializing back multiple pages of goodreads data. We refer to this as a single 'chunk'. Data is written back 
     * without ever being placed into a POJO (which may well be a useful intermediary for the server). Right now,
     * the server's only job is to act as a pass-through, though, so this does the trick.
     * @return
     *      The total number of results.
     */
    public int writePage(InputStream inputPage) {
        int totalResults = 0;
        int pageBookCount = 0;
        try {
            if (!pageWritten) {
                writer.startArray();
                pageWritten = true;
            }
            XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(inputPage);
            while (xmlEventReader.hasNext()) {
                XMLEvent event = xmlEventReader.nextEvent();
                if(StaxUtil.isElement(event, "total-results"))
                    // Total results cann't exceed MAX_RESULTS (the maximum number of results the goodreads API
                    // will return).
                    totalResults = Math.min(Integer.parseInt(xmlEventReader.getElementText()), MAX_RESULTS);

                if (StaxUtil.isElement(event, "best_book")) {
                    readBook(xmlEventReader);
                    pageBookCount++;
                }
            }

        } catch (XMLStreamException | IOException e) {
            throw new RequestException(e);
        }
        return totalResults;
    }

    public void end() throws IOException {
        writer.endArray();
    }

    private void readBook(XMLEventReader reader) throws XMLStreamException, IOException {
        writer.startObject();
        XMLEvent xmlEvent = reader.nextEvent();
        int depth = 0;
        while(!xmlEvent.isEndElement() || depth > 0) {
            if(StaxUtil.isElement(xmlEvent, "id")) {
                writer.writeIntProperty("id", Integer.parseInt(reader.getElementText()));
            } else if (StaxUtil.isElement(xmlEvent, "title")) {
                writer.writeProperty("title", reader.getElementText());
            }
            else if(StaxUtil.isElement(xmlEvent, "small_image_url")) {
                writer.writeProperty("smallImageUrl", reader.getElementText());
            }
            else if(StaxUtil.isElement(xmlEvent, "author")) {
                readAuthor(reader);
            }
            else {
                if(xmlEvent.isStartElement()) {
                    depth++;
                }
                if(xmlEvent.isEndElement()) {
                    depth--;
                }
            }
            xmlEvent = reader.nextEvent();
        }
        writer.endObject();
    }

    private void readAuthor(XMLEventReader reader) throws XMLStreamException, IOException {
        XMLEvent event = reader.nextEvent();
        while(!StaxUtil.isElement(event, "name")){
            event = reader.nextEvent();
        }
        writer.writeProperty("author", reader.getElementText());
        while(!event.isEndElement()){
            event = reader.nextEvent();
        }
    }
}
