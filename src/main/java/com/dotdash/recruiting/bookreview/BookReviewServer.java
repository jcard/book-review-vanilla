package com.dotdash.recruiting.bookreview;

import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;

/**
 * Wrapper over the Jetty server. This class is responsible for configuring and running that server, including
 * setting up all handler routings (e.g. '/search', and file resources).
 */
public class BookReviewServer {
    private static BookReviewServer instance = new BookReviewServer();

    private BookReviewServer(){}

    public static BookReviewServer getInstance() {
        return instance;
    }

    private Server server = new Server(ConfigProperties.getInstance().getServerPort());

    public void start() {

        try {
            ResourceHandler resourceHandler = new ResourceHandler();
            resourceHandler.setResourceBase("./src/webapp");

            ContextHandler searchHandler = new ContextHandler();
            searchHandler.setContextPath( "/search" );
            searchHandler.setHandler(new BookSearchHandler());

            HandlerList mainHandler = new HandlerList();
            mainHandler.addHandler(searchHandler);
            mainHandler.addHandler(resourceHandler);

            server.setHandler(mainHandler);

            server.start();
            server.join();
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void stop() {
        // Any resource cleanup to do?
    }
}
