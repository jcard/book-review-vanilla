package com.dotdash.recruiting.bookreview;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.Request;
import org.eclipse.jetty.client.api.Response;
import org.eclipse.jetty.client.util.InputStreamResponseListener;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLEncoder;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Client proxy for the goodreads search API call.
 */
public class BookSearchClient {
    private static final String SEARCH_PATH = "/search/index.xml";

    /**
     * Perform a search using the request provided. This is a blocking (synchronous) but streaming implementation.
     * Using StAX and Jetty Client to do the work. Because this is streaming, we can support using very large chunk
     * sizes on the request without worrying about blowing the top off the server's heap space.
     */
    public void search(BookSearchRequest request, HttpServletResponse response, Writer writer) {
        BookSearchResponseMapper mapper = new BookSearchResponseMapper(writer);

        int startPage = (request.getChunkNumber() * request.getChunkSize()) + 1; // pages are 1-indexed
        int currentPage = startPage;

        int totalResults = doRequest(request, currentPage, mapper);
        currentPage++;
        if(totalResults == -1) {
            throw new RequestException("Call to Goodreads API resulted in a non-200 response code. Aborting request");
        }

        int totalPages = (int) Math.ceil(totalResults / 20d);
        response.addIntHeader("dotdash.pages", totalPages);

        int lastPage = Math.min(totalPages, startPage + request.getChunkSize() - 1);

        while(currentPage <= lastPage) {
            doRequest(request, currentPage, mapper);
            currentPage++;
        }
        try {
            mapper.end();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private int doRequest(BookSearchRequest request, int page, BookSearchResponseMapper mapper) {
        String url;
        try {
            url = String.format("%s?q=%s&page=%s&key=%s",
                    ConfigProperties.getInstance().getGoodreadsBaseUrl() + SEARCH_PATH,
                    URLEncoder.encode(request.getSearchString(), "UTF-8"), page, request.getKey());
        } catch (UnsupportedEncodingException e) {
            throw new RequestException(e);
		}
        HttpClient client = HttpClientFactory.getClient();

        InputStreamResponseListener listener = new InputStreamResponseListener();

        Request httpRequest = client.newRequest(url);
        httpRequest.send(listener);

        try(InputStream input = listener.getInputStream()) {
            return mapper.writePage(input);
        } catch (IOException e) {
            throw new RequestException(e);
        }
    }
}
