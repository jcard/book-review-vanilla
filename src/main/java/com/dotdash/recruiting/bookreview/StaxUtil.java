package com.dotdash.recruiting.bookreview;

import com.sun.org.apache.xpath.internal.SourceTree;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

public class StaxUtil {

    public static boolean isElement(XMLEvent event, String name) {
        return event.isStartElement()
                && event.asStartElement().getName().getLocalPart().equals(name);
    }
}
