package com.dotdash.recruiting.bookreview;

import org.eclipse.jetty.client.HttpClient;

/**
 * Lazy-singleton holding an HTTP-client that can be shared across the application.
 */
public class HttpClientFactory {

    private static class InstanceHolder {
        public static HttpClient instance = new HttpClient();

        static {
            try {
                instance.setFollowRedirects(true);
                instance.start();
            } catch(Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static HttpClient getClient() {
        return InstanceHolder.instance;
    }

    public void destroy() {
        InstanceHolder.instance.destroy();
    }
}
