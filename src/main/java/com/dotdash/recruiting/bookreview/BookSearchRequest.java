package com.dotdash.recruiting.bookreview;

public class BookSearchRequest {
    private String key;
    private int chunkSize;
    private int chunkNumber;
    private String searchString;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getChunkSize() {
        return chunkSize;
    }

    /**
     * The goodreads API returns data in small pages (20 records). Here, a chunkSize is defined as a the number of pages to
     * return in a single request to our server (between 1 and N). Because we are streaming the data through, no cap is
     * being set on the chunkSize size. Realistically, the client could get themselves into trouble, though, if they try to
     * load too much data.
     */
    public void setChunkSize(int chunkSize) {
        this.chunkSize = chunkSize;
    }

    public int getChunkNumber() {
        return chunkNumber;
    }

    public void setChunkNumber(int chunkNumber) {
        this.chunkNumber = chunkNumber;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }
}
