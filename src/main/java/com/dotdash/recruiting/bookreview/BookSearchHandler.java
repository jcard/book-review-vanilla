package com.dotdash.recruiting.bookreview;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BookSearchHandler  extends AbstractHandler {
    // Default logger configuration is used. For a production app, we'd configure a file appender (and probably use
    // something like Logback instead of the JDK logger). Note that the Jetty logger will also log this error to StdErr.
    private static Logger logger = Logger.getLogger("com.dotdash.recruiting.bookreview");

    public void handle(String target, Request baseRequest, HttpServletRequest servletRequest, HttpServletResponse response)
        throws IOException {
        try {
            BookSearchRequest request = new BookSearchRequest();
            request.setKey(ConfigProperties.getInstance().getGoodreadsKey());
            request.setChunkNumber(getIntParameter(servletRequest, "chunkNumber"));
            request.setChunkSize(getIntParameter(servletRequest, "chunkSize"));
            request.setSearchString(getStringParameter(servletRequest, "searchString"));

            response.setContentType("text/json;charset=utf-8");
            response.setStatus(HttpServletResponse.SC_OK); // written before the search to support streaming.

            Writer responseWriter = response.getWriter();
            new BookSearchClient().search(request, response, responseWriter);

            baseRequest.setHandled(true);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occurred processing request", e);
            throw e;
        }
    }

    private int getIntParameter(HttpServletRequest request, String name) {
        String stringParam = getStringParameter(request, name);
        try {
            return Integer.parseInt(stringParam);
        } catch(NumberFormatException e) {
            throw new RequestException("Invalid integer query parameter: " + name + ". Value is: " + stringParam, e);
        }
    }

    private String getStringParameter(HttpServletRequest request, String name) {
        return request.getParameter(name);
    }
}
