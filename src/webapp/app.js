// In a "real" system I would use a single JS file only in the simplest cases. More commonly, components would each
// have their own file and I'd use something like webpack to deal with bundling. Similar to the server-side, I've
// omitted any frameworks to keep things tight and small.

/**
 * 'app' module is intended to encapsulate application logic with no dependencies on the DOM. This has
 * benefits by making things like Unit Testing easier (not to mention the whole separation of concerns thing).
 */
var app = (function () {
    // Arbitrary chunk size. We could play with this value to optimize server performance for throughput,
    // and for client time-to-render.
    var CHUNK_SIZE = 5;
    
    var module = {};

    module.search = function(searchString, progressUpdateFn) {

        return doSearch(searchString, 0).then(function(result) {
            var pages = result[0];
            var chunk = result[1];
            var totalChunks = Math.ceil(pages / CHUNK_SIZE);

            // Build an array of requests, if any more are needed..
            var requestFns = []
            for(var i = 1; i < totalChunks; i++) {
                requestFns.push(function(chunkNumber) {
                    return function() {
                        return doSearch(searchString, chunkNumber);

                    }
                }(i));
            }

            var responses = chunk.slice(0);
            // ..then run them serialy, by converting into a chain of promises. This would have easier
            // if I'd pulled in RxJS and done something like `switchMap`. Observables are awesome!
            return requestFns.reduce(function(p, requestFn, currentIndex) {
                return p.then(function() {
                    return requestFn.call(searchString, currentIndex).then(function(response) {
                        var searchResults = response[1];
                        if (progressUpdateFn) {
                            progressUpdateFn.call(this, calculateProgress(currentIndex, requestFns.length));
                        }
                        responses = responses.concat(searchResults);
                        return responses;
                    });
                });
            }, Promise.resolve(responses)).then(deduplicate);
        });
    };

    function doSearch(searchString, chunkNumber) {
       // Fetch is not supported by IE11 and would need to be polyfilled for old browsers in any "real" case
        // where we wanted to keep using it.
        return fetch('/search?searchString=' + searchString + '&chunkSize=' + CHUNK_SIZE + '&chunkNumber=' + chunkNumber)
            .then(function(response) {
                if (!response.ok) {
                    throw new Error('HTTP error, status = ' + response.status);
                }
                var pages = response.headers.get('dotdash.pages');
                return response.json().then(function(json) {
                    return  [pages, json];
                })
            });
    }

    function calculateProgress(index, totalRequests) {
        return Math.round(((index + 1) / totalRequests) * 100);
    }

    /**
     * Deduplicate books by ID. We end up with duplicated because of weirdness with the Goodreads API.
     */
    function deduplicate(books) {
        var ids = {};
        return books.filter(function(book) {
            if(ids[book.id]) {
                return false;
            } else {
                ids[book.id] = true;
                return true;
            }
        });

    }

    return module;
}());

/**
 * 'ui' Module intended to provide an interface for the UI. This module also interacts directly with the DOM.
 */
var ui = (function (app) {
    var module = {};
    var searchResults,
        sortField = 'title';

    // Element handles - these are static on the page, so we can have fixed handles to them.
    var searchButtonEl = document.getElementById('search-button'),
        searchInputEl = document.getElementById('search-input'),
        searchResultsEl = document.getElementById('search-results'),
        searchTextEl = document.getElementById('search-text'),
        sortEl = document.getElementById('sort-field');

    module.search = function() {
        // There are a number of concerns not properly dealt with wrt. the search. These include:
        // 1) prevent (or cancel) multiple concurrent submissions (still possible despite disabling button).
        // 2) allowing explicit request cancellation. 
        // 3) supporting <= ie11
        // 4) virtual rendering of rows or lazy-loading of images (eg. scrolling pagination)
        // 5) race conditions between validation and search
        searchButtonEl.disabled = true;
        searchResultsEl.innerHTML = '<p>Loading...</p>';
        searchResults = null;

        var inputValue = searchInputEl.value;

        function updateProgress(percentComplete) {
            searchResultsEl.innerHTML = '<p>Loading...' + percentComplete + '%</p>';
        }

        app.search(inputValue, updateProgress).then(function(results) {
            searchResults = results;
            sort(sortField);
            applyResultsToDOM();;
            searchButtonEl.disabled = false;
        }, setServerError);
    };

    module.validateInput = function() {
        var inputValue = searchInputEl.value;
        if(inputValue.length < 3) {
            setError('Please enter at least three characters.');
        } else {
            setError(null);
        }
    };

    module.sort = function() {
        searchButtonEl.disabled = true;
        sortField = sortEl.value;
        sort(sortField);
        applyResultsToDOM();
        searchButtonEl.disabled = false;
    };

    searchButtonEl.onclick = module.search;

    function setError(text) {
        searchButtonEl.disabled = !!text;
        searchTextEl.innerHTML = text;
    }

    function setServerError() {
        // introduces a race condition between validation and server calls that we'll just ignore for now.
        //searchButtonEl.disabled = false;
        searchTextEl.innerHTML = 'An error occurred. Please retry your search';
        searchResultsEl.innerHTML = '';
    }

    function applyResultsToDOM() {
        var results = (searchResults || [])
        var html = '<p>Found ' + results.length + ' records.</p>' + results.map(toTemplate).join('');
        searchResultsEl.innerHTML = html;
    }

    function toTemplate(result) {
        return '<div class="search-result-row">' 
            + '<div><img src="' + result.smallImageUrl + '"></div><div style="margin-left: 20px">'
            + '<b>' + result.title + '</b><br>'
            + result.author
            + '</div></div>'
    }

    function sort(sortField) {
        if(!searchResults) {
            return;
        }
        searchResults = searchResults.sort(function(a, b) {
            return a[sortField].localeCompare(b[sortField]);
        });
    }

    return module;
}(app));